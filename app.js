
var bodyParser = require('body-parser');
var express = require('express')
var app = express();
const ffmpegPath = require('@ffmpeg-installer/ffmpeg').path;
const ffmpeg = require('fluent-ffmpeg');
ffmpeg.setFfmpegPath(ffmpegPath);
const fs = require('fs');
var Jimp = require('jimp');
const spawn = require('child_process').spawn;
app.use(express.static("public"))
app.use(bodyParser.json({limit: '100mb'}));
app.use(bodyParser.urlencoded({limit: '100mb', extended: true}));

app.listen(8080);

app.post('/createVideo' , (req, res)=>{
console.log(req.body);
/// create profile image
Jimp.read(req.body.user.image , (err, lenna) => {
lenna.write(req.body.user.userId+'.png' , (err , data)=>{
const ls = spawn('python', [
   'vid.py', 'clip' , req.body.user.userId+'.png' , req.body.user.userId 
   ]);
ls.stdout.on('data', (data) => {
      console.log(data.toString().replace(/\s/g,''));
      if(data.toString().replace(/\s/g,'') === 'done'){
         console.log('it is really done');
         console.log('start processing');
         ffmpeg('./'+req.body.user.userId+'.mp4')
         .addInput('./beats.mp3')
         .size('1080x1080')
         .output('./public/'+req.body.user.userId+'.mp4')
  .on('progress', function(progress) { 
      console.log('... frames: ' + progress.frames);
   })
  .on('end', function() {
    console.log('Finished processing');
   res.json({status: true , 
   path: 'https://74f19a50.ngrok.io/'+req.body.user.userId+'.mp4'});
  })
  .run();

      }
    });
    
    ls.stderr.on('data', (data) => {
      console.log(`stderr: ${data}`);
    });
    
    ls.on('close', (code) => {
      console.log(`child process exited with code ${code}`);
    });
});
});
});