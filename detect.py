# import the necessary packages
import numpy as np
import argparse
import time
import cv2
import os

# load the COCO class labels our YOLO model was trained on
labelsPath = os.path.sep.join(['./yolo-coco', "coco.names"])
LABELS = open(labelsPath).read().strip().split("\n")

# initialize a list of colors to represent each possible class label
np.random.seed(42)
COLORS = np.random.randint(0, 255, size=(len(LABELS), 3),
	dtype="uint8")

# derive the paths to the YOLO weights and model configuration
weightsPath = os.path.sep.join(['./yolo-coco', "yolov3.weights"])
configPath = os.path.sep.join(['./yolo-coco', "yolov3.cfg"])

# load our YOLO object detector trained on COCO dataset (80 classes)
print("[INFO] loading YOLO from disk...")
net = cv2.dnn.readNetFromDarknet(configPath, weightsPath)
ln = net.getLayerNames()
ln = [ln[i[0] - 1] for i in net.getUnconnectedOutLayers()]

# initialize the video stream, pointer to output video file, and
# frame dimensions
vs = cv2.VideoCapture("./clip.mp4")
writer = None
(W, H) = (None, None)
'''
image = cv2.imread("head1.png", cv2.IMREAD_UNCHANGED)
(wH, wW) = image.shape[:2]

(B, G, R, A) = cv2.split(image)
B = cv2.bitwise_and(B, B, mask=A)
G = cv2.bitwise_and(G, G, mask=A)
R = cv2.bitwise_and(R, R, mask=A)
image = cv2.merge([B, G, R, A])
'''
#orig_mask = image[:, :, 3]
#orig_mask_inv = cv2.bitwise_not(orig_mask)

#imgMustache = image[:, :, 0:3]
#origMustacheHeight, origMustacheWidth = imgMustache.shape[:2]

# try to determine the total number of frames in the video file
try:
	prop = cv2.cv.CV_CAP_PROP_FRAME_COUNT if imutils.is_cv2() \
		else cv2.CAP_PROP_FRAME_COUNT
	total = int(vs.get(prop))
	print("[INFO] {} total frames in video".format(total))

# an error occurred while trying to determine the total
# number of frames in the video file
except:
	print("[INFO] could not determine # of frames in video")
	print("[INFO] no approx. completion time can be provided")
	total = -1

# loop over frames from the video file stream
while True:
	# read the next frame from the file
	(grabbed, frame) = vs.read()
	# if the frame was not grabbed, then we have reached the end
	# of the stream
	if not grabbed:
		print("done ===>")
		break

	gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	# if the frame dimensions are empty, grab them
	if W is None or H is None:
		(H, W) = frame.shape[:2]
      	# construct a blob from the input frame and then perform a forward
	# pass of the YOLO object detector, giving us our bounding boxes
	# and associated probabilities
	blob = cv2.dnn.blobFromImage(frame, 1 / 255.0, (416, 416),
		swapRB=True, crop=False)
	net.setInput(blob)
	start = time.time()
	layerOutputs = net.forward(ln)
	end = time.time()
 
	# initialize our lists of detected bounding boxes, confidences,
	# and class IDs, respectively
	boxes = []
	confidences = []
	classIDs = []
	gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
   	# loop over each of the layer outputs
	for output in layerOutputs:
		# loop over each of the detections
		for detection in output:
			# extract the class ID and confidence (i.e., probability)
			# of the current object detection
			scores = detection[5:]
			classID = np.argmax(scores)
			confidence = scores[classID]
			# filter out weak predictions by ensuring the detected
			# probability is greater than the minimum probability
			if confidence > 0.5:
				# scale the bounding box coordinates back relative to
				# the size of the image, keeping in mind that YOLO
				# actually returns the center (x, y)-coordinates of
				# the bounding box followed by the boxes' width and
				# height
		    	

				box = detection[0:4] * np.array([W, H, W, H])
				(centerX, centerY, width, height) = box.astype("int")
		
				# use the center (x, y)-coordinates to derive the top
				# and and left corner of the bounding box
				x = int(centerX - (width / 2))
				y = int(centerY - (height / 2))
 
				# update our list of bounding box coordinates,
				# confidences, and class IDs
				boxes.append([x, y, int(width), int(height)])
				confidences.append(float(confidence))
				classIDs.append(classID)
            
            # apply non-maxima suppression to suppress weak, overlapping
	# bounding boxes
	idxs = cv2.dnn.NMSBoxes(boxes, confidences, 0.5, 0.3)
	
	
	# ensure at least one detection exists
	if len(idxs) > 0:
		# loop over the indexes we are keeping
		for i in idxs.flatten():
			# extract the bounding box coordinates
			(x, y) = (boxes[i][0], boxes[i][1])
			(w, h) = (boxes[i][2], boxes[i][3])
 
			# draw a bounding box rectangle and label on the frame
			name = LABELS[classIDs[i]]
			
			color = [int(c) for c in COLORS[classIDs[i]]]
			# create overlay
			#roi_gray = gray[y:y + h, x:x + w]
			#roi_color = frame[y:y + h, x:x + w]	

			# take ROI for mustache from background equal to size of mustache image
			#roi = roi_color[y:y+h, x:x+w]
			
			# roi_bg contains the original image only where the mustache is not
			# in the region that is the size of the mustache.
			#roi_bg = cv2.bitwise_and(roi,roi,mask = orig_mask_inv)
			
			# roi_fg contains the image of the mustache only where the mustache is
			#roi_fg = cv2.bitwise_and(mustache,mustache,mask = mask)
			
			# join the roi_bg and roi_fg
			#dst = cv2.add(roi_bg,roi_fg)
			
			# place the joined image, saved to dst back over the original image
			#roi_color[y:y+h, x:x+w] = (255, 255, 0)
			#frame = cv2.cvtColor(frame, cv2.COLOR_BGR2BGRA)
			frame_h, frame_w, frame_c = frame.shape
			#overlay = np.zeros(frame.shape,'uint8')
			#nW = (y-40:y+30)
			#print(nW)
			overlay = np.zeros((frame_h, frame_w, 4), dtype="uint8")
			
			#overlay[y-40:y+130, (x+(w/2))-60:(x+(w/2))+60] = (255, 255, 0)
			#img = cv2.resize(image,(150,150))
			#head_h, head_w, head_c = img.shape
			#print(roi_color.shape)

			cv2.circle(frame, (x+(w/2), y), 60, color, 3)
			#cv2.rectangle(frame, (x, y), (x + w, y + h), color, 2)
			#frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)			
			#cv2.addWeighted(overlay , 1, frame, 1.0, 0, frame)
			#frame = cv2.cvtColor(frame, cv2.COLOR_BGRA2BGR)
			#text = "{}: {:.4f}".format(LABELS[classIDs[i]],
				#confidences[i])
			#cv2.putText(frame, text, (x, y - 5),
			#	cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
            	# check if the video writer is None
	'''
	if writer is None:
		# initialize our video writer
		fourcc = cv2.VideoWriter_fourcc(*"mp4v")
		writer = cv2.VideoWriter("./output.mp4", fourcc, 30,
       (frame.shape[1], frame.shape[0]), True)
 
		# some information on processing single frame
		if total > 0:
			elap = (end - start)
			print("[INFO] single frame took {:.4f} seconds".format(elap))
			print("[INFO] estimated total time to finish: {:.4f}".format(
				elap * total))
 
	# write the output frame to disk
	writer.write(frame)
	'''
	cv2.imshow("frame", frame)
# release the file pointers
print("[INFO] cleaning up...")
#writer.release()
vs.release()