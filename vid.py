import cv2
import numpy as np
import argparse
import time
import os
import sys
print "hey getting started"
sys.stdout.flush()

outputVideo = "output"
inputImage = "1.png"
inputVideo = "clip"

# load the COCO class labels our YOLO model was trained on
labelsPath = os.path.sep.join(['./yolo-coco', "coco.names"])
LABELS = open(labelsPath).read().strip().split("\n")

# initialize a list of colors to represent each possible class label
np.random.seed(42)
COLORS = np.random.randint(0, 255, size=(len(LABELS), 3),
	dtype="uint8")

# derive the paths to the YOLO weights and model configuration
weightsPath = os.path.sep.join(['./yolo-coco', "yolov3.weights"])
configPath = os.path.sep.join(['./yolo-coco', "yolov3.cfg"])

# load our YOLO object detector trained on COCO dataset (80 classes)
net = cv2.dnn.readNetFromDarknet(configPath, weightsPath)
ln = net.getLayerNames()
ln = [ln[i[0] - 1] for i in net.getUnconnectedOutLayers()]


# Create a VideoCapture object and read from input file
# If the input is the camera, pass 0 instead of the video file name
cap = cv2.VideoCapture("./"+inputVideo+".mp4")

image = cv2.imread( inputImage , cv2.IMREAD_UNCHANGED)
orig_mask = image[:, :, 2]
orig_mask_inv = cv2.bitwise_not(orig_mask)

# Convert mustache image to BGR
# and save the original image size (used later when re-sizing the image)
image = image[:, :, 0:3]

(imageH, imageW) = image.shape[:2]

writer = None
(W, H) = (None, None)

# Check if camera opened successfully
if (cap.isOpened()== False): 
  print("Error opening video stream or file")
 
# Read until video is completed
while(cap.isOpened()):
  # Capture frame-by-frame
  ret, frame = cap.read()
  if ret == True:
	if W is None or H is None:
		(H, W) = frame.shape[:2]

	gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	blob = cv2.dnn.blobFromImage(frame, 1 / 255.0, (416, 416),
	swapRB=True, crop=False)
	net.setInput(blob)
	start = time.time()
	layerOutputs = net.forward(ln)
	end = time.time()

	boxes = []
	confidences = []
	classIDs = []

	for output in layerOutputs:
		# loop over each of the detections
		for detection in output:
			# extract the class ID and confidence (i.e., probability)
			# of the current object detection
			scores = detection[5:]
			classID = np.argmax(scores)
			confidence = scores[classID]
			# filter out weak predictions by ensuring the detected
			# probability is greater than the minimum probability
			if confidence > 0.5:
				# scale the bounding box coordinates back relative to
				# the size of the image, keeping in mind that YOLO
				# actually returns the center (x, y)-coordinates of
				# the bounding box followed by the boxes' width and
				# height
		    	
				box = detection[0:4] * np.array([W, H, W, H])
				(centerX, centerY, width, height) = box.astype("int")
				
				# use the center (x, y)-coordinates to derive the top
				# and and left corner of the bounding box
				x = int(centerX - (width / 2))
				y = int(centerY - (height / 2))
 
				# update our list of bounding box coordinates,
				# confidences, and class IDs
				boxes.append([x, y, int(width), int(height)])
				confidences.append(float(confidence))
				classIDs.append(classID)
	
	idxs = cv2.dnn.NMSBoxes(boxes, confidences, 0.5, 0.3)
	
	# ensure at least one detection exists
	if len(idxs) > 0:
		# loop over the indexes we are keeping
		for i in idxs.flatten():
			# extract the bounding box coordinates
			(x, y) = (boxes[i][0], boxes[i][1])
			(w, h) = (boxes[i][2], boxes[i][3])
 
			# draw a bounding box rectangle and label on the frame
			name = LABELS[classIDs[i]]
			
			color = [int(c) for c in COLORS[classIDs[i]]]
			# create overlay
			frame_h, frame_w, frame_c = frame.shape
			roi_gray = gray[y:y + h, x:x + w]
			roi_color = frame[0:frame_h, 0:frame_w]

			# take ROI for mustache from background equal to size of mustache image
			#roi = roi_color[y:y+h, x:x+w]
			
			# roi_bg contains the original image only where the mustache is not
			# in the region that is the size of the mustache.
			#roi_bg = cv2.bitwise_and(roi,roi,mask = orig_mask_inv)
			#print(roi_color.shape)
			# roi_fg contains the image of the mustache only where the mustache is
			#roi_fg = cv2.bitwise_and(mustache,mustache,mask = mask)
			
			# join the roi_bg and roi_fg
			#dst = cv2.add(roi_bg,roi_fg)
			# place the joined image, saved to dst back over the original image
			img = cv2.resize(image, (120, (y+130) - (y-40) ) )
			try:
				roi_color[y-40:y+130, (x+(w/2))-60:(x+(w/2))+60] = img
			except:
				print("error")
				sys.stdout.flush()
				continue

			#overlay = np.zeros((frame_h, frame_w, 3), dtype="uint8")
			#overlay[y-40:y+130, (x+(w/2))-60:(x+(w/2))+60] = img
			
			#head_h, head_w, head_c = img.shape
			#cv2.circle(frame, (x+(w/2), y), 60, color, 3)
			#cv2.rectangle(frame, (x, y), (x + w, y + h), color, 2)
			
			#frame = cv2.cvtColor(frame, cv2.COLOR_BGRA2BGR)
			#cv2.addWeighted(overlay , 1, frame, 1.0, 0, frame)
			
	
	if writer is None:
		# initialize our video writer
		fourcc = cv2.VideoWriter_fourcc(*"mp4v")
		writer = cv2.VideoWriter("./"+outputVideo+".mp4", fourcc, 30,
       (frame.shape[1], frame.shape[0]), True)
 
		# some information on processing single frame
		# if total > 0:
		# 	elap = (end - start)
		# 	print("[INFO] single frame took {:.4f} seconds".format(elap))
		# 	print("[INFO] estimated total time to finish: {:.4f}".format(
		# 		elap * total))
	# write the output frame to disk
	#writer.write(frame)
	






    # Display the resulting frame
	cv2.imshow('Frame',frame)
 
    # Press Q on keyboard to  exit
	if cv2.waitKey(25) & 0xFF == ord('q'):
		break
 
  # Break the loop
  else: 
    break
 
# When everything done, release the video capture object
cap.release()
print("done")
sys.stdout.flush()
writer.release()
# Closes all the frames
cv2.destroyAllWindows()